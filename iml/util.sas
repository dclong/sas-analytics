/* 
@param x a matrix.
@return a matrix m such that m[i, j] = i 
*/
start row(x);  
   return(repeat(t(1:nrow(x)), 1, ncol(x)));
finish;

/* 
@param x a matrix.
@return a matrix m such that m[i, j] = j 
*/
start col(x);  
   return(repeat(1:ncol(x), nrow(x)));
finish;

/*
Get the number of elements in a matrix.
@param x a matrix.
@return the number of elements in the matrix x.
*/
start size(x);
    return(nrow(x) * ncol(x));
finish;

/*
Get the number of elements in a matrix.
@param x a matrix.
@return the number of elements in the matrix x.
*/
start numel(x);
    return(nrow(x) * ncol(x));
finish;

/**
Missing values due to differencing are removed, 
which is different from the built-in function dif.

@param x the vector/matrix to be differenced.

@param k the distance of differencing.

@return the difference in a COLUMN vector (matrix with 1 column).
*/
start diff_no_missing(x, k);
    n = nrow(x) * ncol(x);
    return(x[(1+k):n] - x[1:(n-k)]);
finish;

/**
Combine two matrices by columns, i.e., concatenate two matrices horizontally.
This function mimic the R-style cbind function.
@param x a matrix.
@param y another matrix.
@return the combined matrix.
*/
start cbind(x, y);
    return(insert(x, y, 0, ncol(x)+1));
finish;
/**
Combine two matrices by rows, i.e., concatenate two matrices vertically.
This function mimic the R-style rbind function.
@param x a matrix.
@param y another matrix.
@return the combined matrix.
*/
start rbind(x, y);
    return(insert(x, y, nrow(x)+1, 0));
finish;

/*
Calculates the yearly probability of default from quarterly probability of default.

@param p_quarter a matrix/vector (best a column vector) containing quarterly PDs.

@return a matrix/vector containing corresponding yearly PDs.
*/
start pd_q2y(p_quarter);
    p = 1 - (1-p_quarter)##4;
    return(p);
finish;

/*
Calculates the yearly probability of default from quarterly probability of default.

@param p_year a matrix/vector (best a column vector) containing yearly PDs.

@return a matrix/vector with the same dimension (as the input) containing corresponding quarterly PDs.
*/
start pd_y2q(p_year);
    p = 1 - (1-p_year)##0.25;
    return(p);
finish;

/*
Calcuate growth of time series x.

@param x a matrix/vector (best to be a column vector).

@param lags the lags to use.

*/
start growth(x, lags);
    g = x/shift(x, lags, .) - 1;
    return(g);
finish;
/*
Convert an array of quarterly growth to yearly growth.

@param g_quarter a matrix/vector (best a column vector) containing quarterly growth rates.

@return a matrix/vector with the same dimension (as the input) containing corresponding yearly growth rates.
*/
start growth_q2y(g_quarter);
    g = (1+g_quarter) ## 4 - 1;
    return(g);
finish;

/*
Convert an array of yearly growth to quarterly growth.

@param g_year a matrix/vector (best a column vector) containing yearly growth rates.

@return a matrix/vector with the same dimension (as the input) containing corresponding quarterly growth rates.
*/
start growth_y2q(g_year);
    g = (1+g_year) ## 0.25 - 1;
    return(g);
finish;
/*
Get rid of leading (in chronological order) records containing missing values.

@param data the original data matrix.
It must contain a column indicating whether a record is historical (0) or secnario (1) record.

@param scenario_index the index/name of column 
which indicator whether a record is historical (0) or scenario (1) record.
*/
start ltrim_missing(data, scenario_index);
    scenario = data[, scenario_index];
	nomissing = (data ^= .)[, ><];
    keep = nomissing | scenario;
    return(data[loc(keep), ]);
finish;

/*
Get rid of leading (in chronological order) records containing missing values.
This module does not require the original data matrix to contain a column 
indicating whether a record is historical or scenario record.
It is more convenient that the sister module ltrim_missing but riskier.

@param data the original data matrix.
*/
start ltrim_missing_risky(data);
	keep = (data ^= .)[, ><]; 
    keep = (cusum(keep)>0);
    return(data[loc(keep), ]);
finish;

/*
Calculate PD scalors for forecasting.

@param dr_stressed The stressed default rate. 
This is calculated via simulation.

@param dr_actual the last (actual) observed default rate.

@param moc the marginal of conservativeness.
*/
start pd_scalar(dr_stressed, dr_actual, moc);
    return(dr_stressed / dr_actual * moc);
finish;

/*
Calculate the cumulative maximum of a vector/matrix.

@param x a vector or matrix.

@return a vector/matrix with the same dimension as x containing the cumulative maximum values.

*/
start cumax(x);
    cmax = x;
    n = nrow(x) * ncol(x);
    do i = 2 to n by 1;
        cmax[i] = max(cmax[i-1], x[i]);
    end;
    return(cmax);
finish;

/*
Calculate the sliding window maximum of a vector/matrix.

@param x a vector or matrix.

@param window the length of the window, which is a positive integer.

@return a vector/matrix with the same dimension as x containing the sliding window maximum values.

*/
start sliding_window_maximum(x, window);
    swm = x;
    n = nrow(x) * ncol(x);
    do i = 2 to n by 1;
        begin_index = max(1, i-window+1);
        swm[i] = max(x[begin_index:i]);
    end;
    return(swm);
finish;

/*
Calculate the sliding window minimum of a vector/matrix.

@param x a vector or matrix.

@param window the length of the window, which is a positive integer.

@return a vector/matrix with the same dimension as x containing the sliding window minimum values.

*/
start sliding_window_minimum(x, window);
    swm = x;
    n = nrow(x) * ncol(x);
    do i = 2 to n by 1;
        begin_index = max(1, i-window+1);
        swm[i] = min(x[begin_index:i]);
    end;
    return(swm);
finish;

/*
Calculate the cumulative minimum of a vector/matrix.

@param x a vector or matrix.

@return a vector/matrix with the same dimension as x containing the cumulative minimum values.

*/
start cumin(x);
    cmin = x;
    n = nrow(x) * ncol(x);
    do i = 2 to n by 1;
        cmin[i] = min(cmin[i-1], x[i]);
    end;
    return(cmin);
finish;

/*
Shift a vector to the right and pad the vancy with the specified values.

@param x the vector/matrix to be shifted to the right.

@param k the number of positions to shift.

@param padding the value used for padding.
There are different ways to pad vacancies when multiple padding values are supplied,
so this function module does not deal with that to avoid issues.

@return a vector/matrix with elements shift to the right.

@note the element(s) on the right end will be dropped due to shifting.
*/
start shift_right(x, k, padding);
    x_len = nrow(x) * ncol(x);
    shift = x;
    do i = k+1 to x_len by 1;
        shift[i] = x[i-k];
    end;
    do i = 1 to k by 1;
        shift[i] = padding;
    end;
    return(shift);
finish;

/*
Shift a vector to the left and pad the vancy with the specified values.

@param x the vector/matrix to be shifted to the left.

@param k the number of positions to shift.

@param padding the value(s) used for padding.

@return a vector/matrix with elements shift to the left.

@note the element(s) on the right end will be dropped due to shifting.
*/
start shift_left(x, k, padding);
    x_len = nrow(x) * ncol(x);
    shift = x;
    do i = (x_len-k) to 1 by -1;
        shift[i] = x[i+k];
    end;
    do i = 1 to k by 1;
        shift[i] = padding;
    end;
    return(shift);
finish;

/*
Shift a vector to the left/right and pad the vancy with the specified values.

@param x the vector/matrix to be shifted to the left/right.

@param k the number of positions to shift.
If k>0, it is shifted to the right;
otherwise, it is shifted to the left.

@param padding the value(s) used for padding.

@return a vector/matrix with elements shift to the left.

@note elements will be dropped due to shifting.
*/
start shift(x, k, padding);
    if k>=0 then do;
        return(shift_right(x, k, padding));
    end;
    return(shift_left(x, -k, padding));
finish;

/*
Check if a matrix is a column vector.

@param x the matrix to be checked.

@return 1 if the matrix is a column vector; 0 otherwise.
*/
start is_column_vector(x);
    return(ncol(x)=1);
finish;

/*
Check if a matrix is a row vector.

@param x the matrix to be checked.

@return 1 if the matrix is a row vector; 0 otherwise.
*/
start is_row_vector(x);
    return(nrow(x)=1);
finish;

/**
Check if a matrix is actually a scalar.

@param x a matrix.

@return 1 if the matrix actually a scalar; 0 otherwise.
*/
start is_scalar(x);
    return(nrow(x)=1 & ncol(x)=1);
finish;

/*
Cap and floor a matrix/vector.
Notice that a YEARLY (not for quarterly) PD is often capped at 1 and floored at 3bps. 

@param x a matrix/vector to be capped and/or floored.

@param cap the maximum value of x after capping.

@param floor the minimum value of x after flooring.

@return a matrix/vector with elements inside [floor, cap]
*/
start cap_floor(x, cap, floor);
    _x = choose(x>cap, cap, x);
    _x = choose(_x<floor, floor, _x);
    return(_x);
finish;

/*
Make the value of a vector/matrix stays at the specified value if ever hit it.

@param x a vector/matrix of value.

@param value a value to be hit.

@return a vector/matrix with the same dimension with x whose values after hit is set to value.
*/
start hit_and_stay(x, value);
    _x = x;
    n = nrow(_x) * ncol(_x);
    hit = 0;
    do i = 1 to n by 1;
        if _x[i] >= value | hit then do;
            hit = 1;
            _x[i] = value;
        end;
    end;
    return(_x);
finish;

/*
Format the output of percentage. 

@param description a description of the percentage.

@param numerator the numerator when calculating the ratio (percentage).

@param denominator the denominator when calculating the ratio (percentage).
*/
start format_percentage(description, numerator, denominator, accuracy);
    return(cat(description, numerator, "/", denominator, "=", round(numerator/denominator*100, accuracy), "%"));
finish;

/*
Check characteristic of a COLUMN vector variable.

@param v a COLUMN vector variable.
*/
start check_variable(v, vname);
    print(cat("------------------------------------------------------------ Checking Variable ", vname, " -------------------------------------------------------------------"));
    print(cat("Data Type: ", type(v)));
    /* total number of observations */
    n = nrow(v) * ncol(v);
    print(cat("Total # of records: ", n));
    /* uniqueness */
    v_unique = unique(v);
    n_unique = nrow(v_unique) * ncol(v_unique);
    if n_unique=n then do;
        print("The values are unique.");
    end; else do;
        print(catx(" ", "There are", n_unique, "unique values."));
        n_rep = n/n_unique;
        if n_unique<=20 | (n_unique<100 & n_rep>=100) then do;
            /* frequency table */
            print(cat("Frequency Distribution of Values of ", vname));
            create work._2030106198812037 from v;
            append from v;
            close work._2030106198812037;
            submit;
            proc freq data=work._2030106198812037;
            run;
            endSubmit;
        end;
    end;
    if type(v)="N" then do;
        /* missing values */
        print(format_percentage("% of missing values: ", sum(v=.), n, 0.01));
        /* 0's */
        print(format_percentage("% of zeros: ", sum(v=0), n, 0.01));
        /* positive numbers */
        print(format_percentage("% of positive values: ", sum(v>0), n, 0.01));
        /* negative numbers */
        print(format_percentage("% of negative values: ", sum(v<0 & v^=.), n, 0.01));
        /* univariate analysis */
        print(cat("Univariate Analysis of Values of ", vname));
        create work._2030106198812037 from v;
        append from v;
        close work._2030106198812037;
        submit;
        ods exclude Frequencies;
        proc univariate data=work._2030106198812037;
            /* var col1; */
        run;
        endSubmit;
    end; else if type(v) = "C" then do;
        /* missing values */
        print(format_percentage("% of missing values: ", sum(v=""), n, 0.01));
        /*---------------------------------------------------------------------------------------------------*/
        n_char = choose(v="", 0, length(v));
        leading = (substr(v, 1, 1)=" ");
        trailing = (substr(v, choose(n_char=0, 1, n_char), 1)=" ");
        /* leading white spaces */
        print(format_percentage("% of records with leading white spaces: ", sum(leading), n, 0.01));
        /* trailing white spaces */
        print(format_percentage("% of records with trailing white spaces: ", sum(trailing), n, 0.01));
        /* leading and trailing white spaces */
        print(format_percentage("% of records with both leading and trailing white spaces: ", sum(leading & trailing), n, 0.01));
        /*---------------------------------------------------------------------------------------------------*/
        /* length of characters */
        print(cat("Frequency Distribution of Length of ", vname));
        create work._2030106198812037 from n_char;
        append from n_char;
        close work._2030106198812037;
        submit;
        proc freq data=work._2030106198812037;
        run;
        endSubmit;
        /*---------------------------------------------------------------------------------------------------*/
        /* numbers in string */
        n_num = countc(v, "0123456789");
        n_nonnum = n_char - n_num;
        /* the overall ratio of numbers */
        print(format_percentage("% of character records with numbers inside: ", sum(n_num>0), n, 0.01));
        print(format_percentage("% of number characters among all characters: ", sum(n_num), sum(n_char), 0.01));
        /* frequency distribution of n_num */
        print(cat("Frequency Distribution of Number of Numbers in ", vname));
        create work._2030106198812037 from n_num;
        append from n_num;
        close work._2030106198812037;
        submit;
        proc freq data=work._2030106198812037;
        run;
        endSubmit;
        print(cat("Frequency Distribution of Number of Non-numbers in ", vname));
        create work._2030106198812037 from n_nonnum;
        append from n_nonnum;
        close work._2030106198812037;
        submit;
        proc freq data=work._2030106198812037;
        run;
        endSubmit;
        /* @TODO: check position of numbers in characters */
        /* check what the non-number characters */
        if sum(n_num)/sum(n_char) >= 0.75 then do;
            /* @TODO: find distribution of non-number characters*/
        end;
    end; else do;
        print("Unknown data type!");
    end;
finish;
