/* 
Shock a variable by a certain quantity.  
The standard deviation is used as the unit 
and the total quantity can be controled via the argument "magnifier".
Currently, only 1 variable can be shocked at a time.
*/
%macro shock(input_data=, variable=, output_data=, magnifier=1);
    proc sql;
        select 
            std(&variable.) into :standard_deviation
        from
            &input_data.
        ;
    quit;
    /* I don't think the following is right ... */
    data &output_data.;
        set &input_data.;
        &variable. = &variable. + magnifier * &standard_deviation.;
    run;
%mend shock;
