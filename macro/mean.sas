/*
@author Ben Chuanlong Du
@description Calculate the mean of a column in a dataset. 
@param input_data the input dataset.
@param variable the variable in the input dataset whose mean is to be calculated.
@param group_by the grouping variable/columns.
@param lib
@param clean
@write 
*/
%macro mean(input_data=, variable=, group_by=, lib=work, clean=True);
    proc sql;
        select
            &group_by.,
            mean(&variable.) as average
        from 
            &input_data.
        group by
            &group_by.
        order by
            &group_by.
        ;
    quit;
%mend mean;
