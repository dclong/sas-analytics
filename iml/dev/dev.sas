
/**
Check the dimension of matrix x.
If x is not of row * col,
print an error message into the SAS log.
@TODO: 
*/
start check_dimension(x, row, col, name);
    if nrow(x) != row or ncol(x) != col then do;
        %put ERROR: the argument ...?;
        /* how should I use name in the error message?*/
        stop;
    end;
    /*
    if nrow(ead) != 13 or ncol(ead) != 1 then do;
        %put ERROR: the argument ead must be a column vector with length 13, i.e., a matrix of 13 * 1.;
        stop;
    end;
    if nrow(lgd) != 13 or ncol(lgd) != 1 then do;
        %put ERROR: the argument lgd must be a column vector with length 13, i.e., a matrix of 13 * 1.;
        stop;
    end;
    */
finish;
/*
@TODO: 
*/
start check_vector(x, name);
    stop;   
finish;

/*
Similar the built-in lag function but without the missing values due to lag.
*/
start lag_no_missing(x);

finish;

start visualize_scenario(y, b, a, s);
finish;

start visualize_backtesting(y, y_pred);
/*
	if it is easier to use proc sgplot, etc. use them directly!!!
	or macro can help here ...
    */
finish;

start visualize_fitting();
finish;

start visualize_predictors(x1, x2);
/*
	x1 being realized predictors
	x2 being scenario predictors
	for each pair of variables, visualize them 
	you have to do this for each predictor
    */
finish;
