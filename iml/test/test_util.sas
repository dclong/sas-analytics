
/*
Note that this code returns the same result as the example in the Excel
except the second element.
I believe the result in the example is incorrect.
*/

proc iml;
	%include "~/sas/util.sas";
    /*****************************************************************/
    %put Testing function module "row" ...;
    if row(1) ^= 1 then do;
        print("Error: testing row failed.");
    end;
    if row({1 2, 3 4}) ^= {1 1, 2 2} then do;
        print("Error: testing row failed.");
    end;
    /*****************************************************************/
    %put Testing function module "col" ...;
    if col(1) ^= 1 then do;
        print("Error: testing col failed.");
    end;
    if col({1 2, 3 4}) ^= {1 2, 1 2} then do;
        print("Error: testing col failed.");
    end;
    /*****************************************************************/
    %put Testing function module "diff_no_missing" ...;
    if diff_no_missing({1 3 9 4 0}, 1) ^= {2, 6, -5, -4} then do;
        print("Error: testing diff_no_missing failed");
    end;
    if diff_no_missing({1 3 9 4 0 12 7}, 3) ^= {3, -3, 3, 3} then do;
        print("Error: testing diff_no_missing failed");
    end;
    /*****************************************************************/
    %put Testing function module "cbind" ...;
    if cbind(1,2) ^= {1 2} then do;
        print("Error: testing cbind failed");
    end;
    if cbind({1 1, 2 2}, {1 2, 3 4}) ^= {1 1 1 2, 2 2 3 4} then do;
        print("Error: testing cbind failed");
    end;
    /*****************************************************************/
    %put Testing function module "rbind" ...;
    if rbind(1,2) ^= {1, 2} then do;
        print("Error: testing rbind failed");
    end;
    if rbind({1 1, 2 2}, {1 2, 3 4}) ^= {1 1, 2 2, 1 2, 3 4} then do;
        print("Error: testing rbind failed");
    end;
    /*****************************************************************/
    %put Testing function module "pd_q2y" ...;
    if pd_q2y(0) ^= 0 then do;
        print("Error: testing pd_q2y failed");
    end;
    if pd_q2y(1) ^= 1 then do;
        print("Error: testing pd_q2y failed");
    end;
    if pd_q2y(0.5) ^= 0.9375 then do;
        print("Error: testing pd_q2y failed");
    end;
    /*****************************************************************/
    %put Testing function module "pd_y2q" ...;
    if pd_y2q(0) ^= 0 then do;
        print("Error: testing pd_y2q failed");
    end;
    if pd_y2q(1) ^= 1 then do;
        print("Error: testing pd_y2q failed");
    end;
    if pd_y2q(0.9375) ^= 0.5 then do;
        print("Error: testing pd_y2q failed");
    end;
    /*****************************************************************/
    %put Testing function module "growth" ...;
    if growth({1 2}, 1) ^= {. 1} then do;
        print("Error: testing growth failed");
    end;
    if growth({1 0}, 1) ^= {. -1} then do;
        print("Error: testing growth failed");
    end;
    if growth({1 2 3 4}, 2) ^= {. . 2 1} then do;
        print("Error: testing growth failed");
    end;
    /*****************************************************************/
    %put Testing function module "cumax" ...;
    if cumax({1 2 0}) ^= {1 2 2} then do;
        print("Error: testing cumax failed");
    end;
    if cumax({1 2 0 9 8}) ^= {1 2 2 9 9} then do;
        print("Error: testing cumax failed");
    end;
    /*****************************************************************/
    %put Testing function module "cumin" ...;
    if cumin({1 2 0}) ^= {1 1 0} then do;
        print("Error: testing cumin failed");
    end;
    if cumin({1 2 0 9 8}) ^= {1 1 0 0 0} then do;
        print("Error: testing cumin failed");
    end;
    /*****************************************************************/
    %put Testing function module "shift_right" ...;
    if shift_right({1 2 0}, 1, .) ^= {. 1 2} then do;
        print("Error: testing shift_right failed");
    end;
    if shift_right({1 2 0 9 8}, 3, 1000) ^= {1000 1000 1000 1 2} then do;
        print("Error: testing shift_right failed");
    end;
    /*****************************************************************/
	%put Testing function module "shift_left" ...;
    if shift_left({1 2 0}, 1, .) ^= {2 0 .} then do;
        print("Error: testing shift_left failed");
    end;
    if shift_left({1 2 0 9 8}, 3, 1000) ^= {9 8 1000 1000 1000} then do;
        print("Error: testing shift_left failed");
    end;
    /*****************************************************************/
	%put Testing function module "shift" ...;
    if shift({1 2 0}, 1, .) ^= {. 1 2} then do;
        print("Error: testing shift failed");
    end;
    if shift({1 2 0 9 8}, 3, 1000) ^= {1000 1000 1000 1 2} then do;
        print("Error: testing shift failed");
    end;
    if shift({1 2 0}, -1, .) ^= {2 0 .} then do;
        print("Error: testing shift failed");
    end;
    if shift({1 2 0 9 8}, -3, 1000) ^= {9 8 1000 1000 1000} then do;
        print("Error: testing shift failed");
    end;
quit;
