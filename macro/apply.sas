/*
@TODO: write a more general macro which allows you to specify different functions to apply to a column
you will need to breakdown the functions 
*/
%macro apply(input_data=, variable=, funs=, group_by=, lib=work, clean=True);
    proc sql;
        select
            &group_by.,
            &funs(variable) as average
        from 
            &input_data.
        group by
            &group_by.
        order by
            &group_by.
        ;
    quit;
%mend apply;
