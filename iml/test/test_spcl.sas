
/*
Note that this code returns the same result as the example in the Excel
except the second element.
I believe the result in the example is incorrect.
*/
proc iml;
	%include "~/sas/spcl.sas";
	ead = {12000 14000 20000 	25000 	35000 	42000 	48000 	52000 	55000 	56000 	57000 	58000 	60000};
	lgd = {0.4	0.35	0.4	0.42	0.5	0.55	0.53	0.52	0.5	0.45	0.4	0.35	0.3};
	print ead;
	print lgd;
	s = spcl(ead, lgd, 4);
	print s;

quit;

