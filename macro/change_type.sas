
/*
@author Ben Chuanlong Du
@description Convert a character column to numeric.
Notice that this macro can only convert 1 column at a time. 
It is certainly desirable to make it support converting multiple columns at the same time,
but it is a little hassle ...
@param input_data A 2-level name (including library name) of an input dataset, 
e.g., work.fit.
@param column the name of the character column to be converted to numeric.
@param format the format of the numeric column, usually "n." where n is a positive number.
@param output_data A 2-level name (including library name) of an output dataset, 
e.g., work.fit.
*/
%macro char2num_1(input_data=, column=, format=, output_data=);
    %local temp_column;
    %let temp_column = _9180222577650411375887992797772;
    data &output_data.;
        set &input_data.;
        &temp_column. = input(&column., &format.);
        drop &column.;
        rename &temp_column. = &column.;
    run;
%mend;

%macro char2num(input_data=, vars=, output_data=, lib=Work, clean=True);
    %local vars_temp;
    %let vars_temp = _9151039851243483265934710175559;
    data &lib..&vars_temp;
        set &vars;
        temp_name = temp_name();
        /* rename = cat(strip(temp_name), "=", strip(name)); */
    run;
    proc sql noPrint;
        select count(*) into :n from &lib..&vars_temp;
        select name into :name_1 - :name_%left(&n) from &lib..&vars_temp;
        select format into :format_1 - :format_%left(&n) from &lib..&vars_temp;
        select temp_name into :tname_1 - :tname_%left(&n) from &lib..&vars_temp;
    run;
    %local i;
    data &output_data.;
        set &input_data.;
        %do i = 1 %to &n %by 1;
            &&tname_&i = input(&&name_&i, &&format_&i);
            drop &&name_&i;
            rename &&tname_&i = &&name_&i;
        %end;
    run;
    %if &clean. = True %then %do;
        proc datasets library=&lib. noList;
            delete &vars_temp;
        run;
    %end;
%mend;

%macro num2char(input_data=, vars=, output_data=, lib=Work, clean=True);
    %local vars_temp;
    %let vars_temp = _7327604824666279267691584918606;
    data &lib..&vars_temp;
        set &vars;
        temp_name = temp_name();
        /* rename = cat(strip(temp_name), "=", strip(name)); */
    run;
    proc sql noPrint;
        select count(*) into :n from &lib..&vars_temp;
        select name into :name_1 - :name_%left(&n) from &lib..&vars_temp;
        select format into :format_1 - :format_%left(&n) from &lib..&vars_temp;
        select temp_name into :tname_1 - :tname_%left(&n) from &lib..&vars_temp;
    run;
    %local i;
    data &output_data.;
        set &input_data.;
        %do i = 1 %to &n %by 1;
            length &&tname_&i $32.;
            if &&name_&i = . then do;
                &&tname_&i = "";
            end; else do;
                &&tname_&i = put(&&name_&i, &&format_&i);
            end;
            drop &&name_&i;
            rename &&tname_&i = &&name_&i;
        %end;
    run;
    %if &clean. = True %then %do;
        proc datasets library=&lib. noList;
            delete &vars_temp;
        run;
    %end;
%mend;

