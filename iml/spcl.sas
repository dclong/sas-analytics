/* include dependency code */
%include "~/sas/iml/util.sas";
%include "~/sas/iml/ia.sas";

/*
@param LGD a column vector containing LGD of the current month AND prejected months.

@param EAD_0 EAD of the current month.

@param formation a matrix containing formations for each account 
with rows being accounts and columns being time in quarter in order.

@param default a row/column vector indicating whether each account is defaulted or not.

@param actual_workout_period a row/column vector containing the workout period 
for actually defaulted retail accounts.

@param perform_workout_period a row/column vector containing the workout period 
for retail accounts that are stressed into default.

@return a matrix containing break-down SPCL for each account 
with rows corresponding to accounts. 

Each row contains break-down SPCL for the corresponding account.
First 1/3: workout SPCL for an actual defaulted retail account; 
0's if performing.

Second 1/3: SPCL for a performing retail account in each month; 
0's if the account is defaulted in the current (t=0) month.

Last 1/3: workout SPCL for a stressed into default retail account; 
0's if the account is deaulted in the current (t=0) month.
*/
start spcl_retail(lgd, ead_0, formation, default, actual_workout_period, perform_workout_period);
    n = nrow(lgd);
    spcl = j(n, ncol(formation)*3, 0);
    do i = 1 to n by 1;
        spcl[i, ] = rowvec(spcl_retail_1(lgd[i, ], ead_0[i], formation[i, ], default[i], actual_workout_period[i], perform_workout_period[i]));
    end;
    return(spcl);
finish;

/*
@param LGD a column vector containing LGD of the current month AND prejected months.

@param EAD_0 EAD of the current month.

@param formation a row/column vector containing formations for this account.

@param default a scalar indicating whether the current account is defaulted or not.

@param actual_workout_period a row/column vector containing the workout period 
for actually defaulted retail accounts.

@param perform_workout_period a row/column vector containing the workout period 
for retail accounts that are stressed into default.

@return a matrix with 3 rows containing breakdown SPCLs.
Row 1: workout SPCL for an actual defaulted retail account; 
0's if performing.
Row 2: SPCL for a performing retail account in each month; 
0's if the account is defaulted in the current (t=0) month.
Row 3: workout SPCL for a stressed into default retail account; 
0's if the account is deaulted in the current (t=0) month.
*/
start spcl_retail_1(lgd, ead_0, formation, default, actual_workout_period, perform_workout_period);
    n = ncol(formation);
    /* create a matrix with 3 rows for returning results */
    spcl = j(3, n, 0);
    if default then do;
        /* calcualte workout SPCL for actual defaulted only */
        spcl[1, ] = rowvec(spcl_workout_actual(lgd, ead_0, actual_workout_period));
    end; else do;
        /* calcualte performing and workout SPCl for stressed into default */
        spcl[2, ] = rowvec(lgd[2:(n+1)]) # rowvec(formation);
        spcl[3, ] = rowvec(spcl_workout_stress(lgd, formation, perform_workout_period)[+, ]);
    end;
    return(spcl);
finish;

/*
@param PD a vector/matrix containing PD of the current month and projected months. 
Note that it must be the quarterly PD NOT yearly.

@param LGD a column vector containing LGD of the current month 
and prejected months.

@param EAD a column vector containing EAD of the current month 
and projected months.

@param workout_period the workout period for wholesale portfolios.

@return a matrix with 3 rows containing breakdown SPCLs.
Row 1: workout SPCL for actual defaulted wholesale portfolios; 0's if performing.
Row 2: SPCL for a performing wholesale portfolio in each month; 0's if the portfolio is defaulted in the current (t=0) month.
Row 3: workout SPCL for stressed into default wholesale portfolio; 0's if the portfolio is deaulted in the current (t=0) month.
*/
start spcl_wholesale(pd, lgd, ead, workout_period, default);
    n = nrow(pd) * ncol(pd);
    /*
     create a matrix with 3 rows for returning results
     row 1: workout spcl for actual default; 0 if performing
     row 2: spcl for performing accounts in each month; 0 if the account is defaulted in the current month
     row 3: workout spcl for stressed into default accounts
    */
    spcl = j(3, n-1, 0);
    if abs(pd[1]-1) < 1e-5 then do;
        /* calcualte workout SPCL for actual defaulted only */
        spcl[1, ] = t(spcl_workout_actual(lgd, ead, workout_period));
    end; else do;
        /* calcualte performing and workout SPCl for stressed into default */
        formation = formation_wholesale(pd, ead);
        spcl[2, ] = as_row_vector(lgd[2:n]) # as_row_vector(formation);
        spcl[3, ] = spcl_workout_stress(lgd, formation, workout_period)[+, ];
    end;
    return(spcl);
finish;

/**
Calcualte workout SPCL.
This is a general function module that works for both retail and wholesale.

@param formation a COLUMN vector containing formations.

@param LGD a column vector containing LGD of the current month 
and prejected months.

@param workout_period the workout period for retail/wholesale portfolios.

@return a (n-1) * (n-1) matrix containing the workout SPCL for performing portfolios/accounts,
where n is the number of elements in LGD.
*/
start spcl_workout_stress(lgd, formation, workout_period);
    n = nrow(formation) * ncol(formation);
    if workout_period <= 0 then do;
        return(j(n, n, 0));
    end;
    lgd_inc = lgd_increment_2(lgd, workout_period);
    spcl = colvec(formation) * rowvec(lgd_inc);
    /* truncation matrix */
    row_index = row(spcl);
    col_index = col(spcl);
    trunc_matrix = (row_index < col_index) & (col_index <= row_index+workout_period);
    /* spcl truncated */
    return(spcl # trunc_matrix);
finish;
/**
calculate SPCL for actually defaulted portfolios/accounts.
This is a general function module that works for both retail and wholesale.

@param EAD_0 EAD of the current month.

@param LGD a column vector containing LGD of the current month 
and prejected months.

@param workout_period the workout period for retail/wholesale portfolios.

@return a COLUMN vector containing SPCL for actually defaulted portfolios/accounts.

*/
start spcl_workout_actual(lgd, ead_0, workout_period);
    n = nrow(lgd) * ncol(lgd) - 1; 
    if workout_period <= 0 then do;
        return(j(n, 1, 0));
    end;
    lgd_inc = lgd_increment_2(lgd, workout_period);
    trunc_vec = j(n, 1, 0);
    trunc_vec[1:workout_period] = 1;
    return(ead_0 * (lgd_inc # trunc_vec));
finish;

/**
Calculate formations for accounts in retail portfolios.

@param pd a matrix containing forecasted PDs 
with rows being accounts and columns corresponding to time (in quarter) in order.
Note that the PDs for the current month should NOT be included.
This argument should typically be the output of the function model "post_pd".

@param ead a matrix containing EADs 
with rows being accounts and columns corresponding to time (in quarter) in order.
Note that the EADs for the current month should NOT be included.

@param default a row/column vector indicating whether each account is in default or not.

@param yearly whether the input PDs are yearly (1) or quarterly (0).
The reason that I keep this argument is because 
it is easy for people to forget that 
they should use the quarterly PDs here rather than yearly PDs.
Notice that if yearly PDs is used, it will automatically be capped (at 1) and floored (at 3 bps).

@return a matrix with the same dimension as the input PD argument containing calcualted formations.
The rows correspond to accounts in the same order as input arguments 
and columns correspond to time (in quarter) in order.

*/
start formation_retail(pd, ead, default, yearly);
    n = nrow(pd);
    formation = j(n, ncol(pd), 0);
    do i = 1 to n by 1;
        if default[i] then do;
            formation[i, ] = 0;
        end; else do;
            _pd = pd[i, ];
            if yearly then do;
                _pd = pd_y2q(cap_floor(_pd, 1, 0.03/100));
            end;
            _ead = ead[i, ];
            formation[i, ] = rowvec(formation_retail_1(_pd, _ead));
        end;
    end;
    return(formation);
finish;

/*
Calculate the formations for a retail portfolio account.
Note: this implementation is based on the assumption that F_0 = 0
(and thus F_1 = f_1).

@param PD a vector/matrix containing PD of the projected months. 
Since the calcualtion here does not require PD of the current month,
it should be NOT be included.
Note that it must be the quarterly PD NOT yearly.

@param EAD a vector/matrix containing EAD of the projected months.
Since the calculation here does not require EAD of the current month,
it should NOT be included.

@return a COLUMN vector containing retail formations for the corresponding account.
Note that since F_0 is 0 and is not used in the calculation of SPCL_workout, 
it is omitted.
That is if PD/EAD has n values, the length of the returned vector is n-1.

@TODO: require pd and ead to be column vector!!!
*/
start formation_retail_1(pd, ead);
    n = nrow(pd) * ncol(pd);
    /* calculate formations */
    f = ead # pd;
    n = nrow(f) * ncol(f);
    form = j(n, 1, 0);
    if ead[1]>0 then do;
        form[1] = f[1]; 
    end;
    cum_form = j(n, 1, form[1]);
    do t = 2 to n by 1;
        if cum_form[t-1]+f[t] <= ead[t] then do;
            form[t] = f[t];
        end;else do;
            form[t] = max(ead[t]-cum_form[t-1], 0);
        end;
        cum_form[t] = cum_form[t-1] + form[t];
    end;
    return(form);
finish;

/*
Calculate the formations for wholesale portfolios.

@param EAD a vector/matrix containing EAD of the current month and projected months.

@return a COLUMN vector containing wholesale formations.
Note that we don't calculate formation for the current month, 
so if EAD has n values, the length of the returned vector is n-1.
*/
start formation_wholesale(ead);
    return(diff_no_missing(ead, 1));
finish;

/*
Calculate the LGD increment, i.e., the max(LGD_{t,y,s} - f(LGD_{t-1,y,s}), 0) part.
This is a general function module that works for both retail and wholesale.

To be specifically, this function module calculates max(LGD_{t,y,s} - LGD_{t-1,y,s}, 0),
which is the way coded in the SAS environment system as of Mar 04, 2015 (and probably exist for a while).

@param LGD a vector/matrix containing LGD in the current month and projected months.

@param a COLUMN vector containing LGD increment. 
If the input has n values, then the returned vector has length n-1.
*/
start lgd_increment_1(lgd);
    increment = diff_no_missing(lgd, 1);
    increment = choose(increment>0, increment, 0);
    return(increment);
finish;

/*
Calculate the LGD increment, i.e., the max(LGD_{t,y,s} - f(LGD_{t-1,y,s}), 0) part.
This is a general function module that works for both retail and wholesale.

To be specifically, this function module calculates max(LGD_{t,y,s} - max{LGD_{t-1,y,s}: in workout period}, 0)
This one sounds to be very complicated ...
Your implementation here is not correct.

@param LGD a vector/matrix containing LGD in the current month and projected months.

@param a COLUMN vector containing LGD increment. 
If the input has n values, then the returned vector has length n-1.
*/
start lgd_increment_2(lgd, workout_period);
    swm = sliding_window_maximum(lgd, workout_period);
    n = nrow(lgd) * ncol(lgd);
    increment = lgd[2:n] - swm[1:(n-1)];
    increment = choose(increment>0, increment, 0);
    return(increment);
finish;


/**
Post - 242 PD assignment for one account in retail portfolio.

@TODO: I don't quite understand what the following item means in Post - 242 assignment documentation.

1. Input PD is mapped to the apropriate master scale rating per PD interval to ensure consistency between PD and rating.

@TODO: another things is that the post -242 PD assignment documentation does not mention anything about 
what we need to do when post_crm_pd is missing while segment_id is not. Is this not possible?

@param segment_id the 10-chracter SEGMENT_ID for this retail account.

@param post_crm_pd the post CRM PD for this retail account.

@param realized_dr_seg the segmentation realized default rate for this retail account.

@param pd_scalar the PD scalars for this retail account.

@param max_realized_dr_seg the maximum realized default rate across all retail portfolios.
It is might be very hard to calculate this, 
but generally speaking you can use the maximum realized default of the portfolio (e.g., IA portfolios) you are dealing with.

@return a vector/matrix with the same dimension as the argument "pd_scalar" 
containing post-processed PDs.

@Note: The logic is slightly different from the post - 242 PD assignment documentation.
Unless you apply the SPCL modules to all retail portfolios at the same time (which I think is hard to do), 
the "max_realized_dr_seg" will be smaller.
However, this is not a big issue at all 
as the corresponding case is rare and the "max_realized_dr_seg" will still be close to the true one.

*/
start post_pd_1(segment_id, post_crm_pd, realized_dr_seg, pd_scalar, max_realized_dr_seg);
    seg_id = strip(upcase(segment_id));
    pd = realized_dr_seg;
    /* 
    @TODO: Mahmoud ask you to apply this 3 bps logic but why isn't it in the post - 242 PD assignment documentation?
    */
    if realized_dr_seg < 0.03/100 then do;
        pd = post_crm_pd;
    end;
    if realized_dr_seg=. & post_crm_pd^=. then do;
        pd = post_crm_pd;
    end;
    if seg_id="" & post_crm_pd^=. then do;
        pd = post_crm_pd;
    end;
    if seg_id="" & post_crm_pd=. then do;
        pd = max_realized_dr_seg;
    end;
    /* forecast pd */
    pd = pd * pd_scalar;
    /* forecasted PD is 1, it stays at 1 */
    pd = hit_and_stay(pd, 1);
    return(pd);
finish;

/**
Post - 242 PD assignment for accounts in a retail portfolio.

@param segment_id a row/column vector containing the 10-chracter SEGMENT_ID's for the acounts 
in a retail portfolio (e.g, the IA portfolio) to be analyzed.

@param post_crm_pd a row/column vector containing the post_crm_pd's 
(which is usually the "PD_PCT" column in the SAS forecasting system) 
for all accounts in the portfolio to be analyzed.
According to the Post - 242 PD assignment rules,
the value is used in place of realized default rate in segments 
if the realized default rate is missing or less than 3 BPS,
or if the segment ID is missing.

@param realized_dr_seg a row/column vector containing the segmentation realized default rates for all accounts 
in the portfolio to be analyzed.

@param pd_scalar a matrix containing PD scalars for all counts (in the portfolio to be analyzed)
with rows being accounts and column being time (in quarter) in order.

@param max_realized_dr_seg the maximum realized default rate across all retail portfolios.
It is might be very hard to calculate this, 
but generally speaking you can use the maximum realized default of the portfolio (e.g., IA portfolios) you are dealing with.

@Note: The logic is slightly different from the post - 242 PD assignment documentation.
Unless you apply the SPCL modules to all retail portfolios at the same time (which I think is hard to do), 
the "max_realized_dr_seg" will be smaller.
However, this is not a big issue at all 
as the corresponding case is rare and the "max_realized_dr_seg" will still be close to the true one.

@return a matrix with the same dimension as the argument "pd_scalar" 
(with rows being accounts and columns being time in quarter in order)
containing post-processed PDs.

@Note This is annualize probability of default not quarterly.
*/
start post_pd(segment_id, post_crm_pd, realized_dr_seg, pd_scalar);
    n = nrow(pd_scalar);
    pd = j(n, ncol(pd_scalar), 0);
    max_realized_dr_seg = max(realized_dr_seg);
    do i = 1 to n by 1;
        pd_i = post_pd_1(segment_id[i], post_crm_pd[i], realized_dr_seg[i], pd_scalar[i, ], max_realized_dr_seg);
        /* cap and floor */
        pd_i = cap_floor(pd_i, 1, 0.03/100);
        pd[i, ] = rowvec(pd_i);
    end;
    return(pd);
finish;


/**
The post LGD assignment (mapped LGD and LGD scalar logic) for retail portfolio accounts.

@param post_crm_lgd a row/column vector containing the post_crm_lgd's 
(which is usually the "LGD_PCT" column in the SAS forecasting system) 
for all accounts in the portfolio to be analyzed.

@param realized_lgd_seg a row/column vector containing the segmentation realized LGD for all accounts 
in the portfolio to be analyzed.

@param lgd_scalar_seg a matrix containing segmentation LGD scalars for all counts (in the portfolio to be analyzed)
with rows being accounts and column being time (in quarter) in order.

@param realized_lgd_port the portfolio-level realized LGD (which is a scalar).

@param lgd_scalar_port a row/column vector containing portfolio-level LGD scalars.

@return a vector containing post LGD of the current AND forecasting months for this retail account.
@return a matrix containing post LGD of the current AND orecasting months 
for the retail accounts
with rows corresponding to accounts.
Each row contains post LGD of the current AND forecasting months 
for the corresponding account.

@Note This is annualize probability of default not quarterly.
*/
start post_lgd(post_crm_lgd, realized_lgd_seg, lgd_scalar_seg, realized_lgd_port, lgd_scalar_port);
    n = nrow(lgd_scalar_seg);
    lgd = j(n, ncol(lgd_scalar_seg)+1, 0);
    do i = 1 to n by 1;
        lgd_i = post_lgd_1(post_crm_lgd[i], realized_lgd_seg[i], lgd_scalar_seg[i, ], realized_lgd_port, lgd_scalar_port);
        lgd[i, ] = rowvec(lgd_i);
    end;
    return(lgd);
finish;

/**
The post LGD assignment (mapped LGD and LGD scalar logic) for one retail portfolio account.

@param post_crm_lgd the post CRM LGD for this retail account.

@param realized_lgd_seg the segmentation realized LGD for this retail account.

@param lgd_scalar_seg the segmentation LGD scalars for this retail account.

@param realized_lgd_port the portfolio-level realized LGD.

@param lgd_scalar_port the portfolio-level LGD scalars.

@return a vector containing post LGD of the current AND forecasting months for this retail account.

@TODO: tried my best, but not sure whether it is close enough to the logic in the SAS system
need to test the results ...

*/
start post_lgd_1(post_crm_lgd, realized_lgd_seg, lgd_scalar_seg, realized_lgd_port, lgd_scalar_port);
    lgd = realized_lgd_seg;
    lgd_scalar = lgd_scalar_seg;
    if realized_lgd_seg=. then do;
        if realized_lgd_port^=. & all(lgd_scalar_port^=.) then do;
            lgd = realized_lgd_port;
            lgd_scalar = lgd_scalar_port;
        end; else do;
            if any(lgd_scalar_port=.) then do;
                lgd = post_crm_lgd;
                lgd_scalar = 1;
            end; else do;
                lgd = post_crm_lgd;
                lgd_scalar = lgd_scalar_port;
            end;
        end;
    end;
    n = nrow(lgd_scalar) * ncol(lgd_scalar);
    result = j(1, n+1, post_crm_lgd);
    result[2:(n+1)] = rowvec(lgd * lgd_scalar);
    return(result);
finish;

