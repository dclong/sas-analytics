
/*
Show the frequency of variable combinations.
Currently only 1 varialbe is supported.
@param input_data The input dataset.
@param group_by grouping variables separated by spaces.
@param output_data The output dataset. 
If blank then no dataset is created.
@param lib
@param clean
@TODO: lib, clean ...
add support when group_by is blank ...
*/
%macro frequency(input_data=, variable=*, group_by=, output_data=, lib=work, clean=True);
    proc sql noPrint;
        select count(*) into :total from &input_data.
        ;
    quit;
    proc sql;
        %if &output_data ^=  %then %do;
            create table &output_data as
        %end;
        select
            &group_by.,
            count(&variable.) as count,
            &total. as total,
            (CALCULATED count) / &total. as ratio
        from	
            &input_data.
        group by
            &group_by.
        ;
    quit;
%mend frequency;
