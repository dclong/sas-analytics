
***************************************************************************************************;
**** macroeconomic data manipulation - transformed macroecnomic variables *************************;
***************************************************************************************************;

/* 
@TODO: make the code more flexible, accept variables names instead of using all variables ... 
*/
%macro transform_macro_econ_variables(input_data=, output_data=, lib=Work, clean=True);
    %local vars;
    %let vars = _8808947860096560495767225058258; 
    proc contents data=&input_data. out=&lib..&vars. ( where = (type=1) ) noPrint; 
    run;
    proc sql;
        select 
            count(name) into :n_var 
        from 
            &vars.
        ;
        select 
            name into :var1-:var&n_var. 
        from 
            &vars.
        ;
    quit;
    data &output_data.;
        set &input_data.;
        %do i = 1 %to &n_var.;
            &&var&i.._l1 = lag(&&var&i);
            &&var&i.._l2 = lag2(&&var&i);
            &&var&i.._l3 = lag3(&&var&i);
            /* ----------------------------------- */
            &&var&i.._qqc = &&var&i - lag(&&var&i);
            &&var&i.._qqc_l1 = lag(&&var&i.._qqc);
            &&var&i.._qqc_l2 = lag2(&&var&i.._qqc);
            &&var&i.._qqc_l3 = lag3(&&var&i.._qqc);
            /* ----------------------------------- */
            &&var&i.._qqg = &&var&i.._qqc / lag(&&var&i);
            &&var&i.._qqg_l1 = lag(&&var&i.._qqg);
            &&var&i.._qqg_l2 = lag2(&&var&i.._qqg);
            &&var&i.._qqg_l3 = lag3(&&var&i.._qqg);
            /* ----------------------------------- */
            &&var&i.._yyc = &&var&i - lag4(&&var&i);
            &&var&i.._yyc_l1 = lag(&&var&i.._yyc);
            &&var&i.._yyc_l2 = lag2(&&var&i.._yyc);
            &&var&i.._yyc_l3 = lag3(&&var&i.._yyc);
            /* ----------------------------------- */
            &&var&i.._yyg = &&var&i.._yyc / lag4(&&var&i);
            &&var&i.._yyg_l1 = lag(&&var&i.._yyg);
            &&var&i.._yyg_l2 = lag2(&&var&i.._yyg);
            &&var&i.._yyg_l3 = lag3(&&var&i.._yyg);
        %end;
    run;
    %if &clean. = True %then %do;
        proc datasets library=&lib. noList;
            delete &vars.;
        run;
    %end;
%mend;
