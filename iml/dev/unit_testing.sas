

%macro unit_test(arg1);
    /* start here */ 
%mend unit_test;



%macro assertEquals(e1, e2);
    %if &e1. = &e2. %then %do;
        %let unit_test_counter = %eval(&unit_test_counter + 1);
    %end; %else %do;
        %put Error: &e1. does not equal &e2.;
    %end;
%mend assertEquals;
