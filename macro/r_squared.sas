/*
@author Benjamin Du
@description Calculate the usual or aggregated R-squared and MSE, etc..
For aggregated R-square and MSE, etc., the mean is used.
@param input_data A 2-level name (including library name) of an input dataset, e.g., work.fit.
@param y the name of the response variable, i.e., y column.
@param yhat Fitted/predicted value of y.
@param output_data A 2-level name (including library name) of an output dataset, e.g., work.r_squared.
@param lib The library for storing temporary datasets.
@param clean If True the temporated datasets generated will be removed; 
otherwise, kept.
*/


%macro r_squared(input_data=, y=, yhat=, group_by=, output_data=, lib=work, clean=True);
    %if &group_by. =  %then %do;
        %r_squared_helper(input_data=&input_data., y=&y., yhat=&yhat., output_data=&output_data., lib=&lib.);
        %return;
    %end;
    %local aggregated;
    %let aggregated = _3719238471955238310190177881117;
    proc sql;
        create table &lib..&aggregated. as
        select
            &group_by as group_by,
            mean(&y) as y,
            mean(&yhat) as yhat
        from 
            &input_data
        group by
            group_by
        ;
    quit;
    %r_squared_helper(input_data=&lib..&aggregated., y=y, yhat=yhat, output_data=&output_data., lib=&lib.);
    %if &clean. = True %then %do;
        proc datasets library=&lib. noList;
            delete &aggregated.;
        run;
    %end;
%mend;

/*
@author Benjamin Du
@description This is a helper macro function for the macro function %r_squared. 
It calculates (the usual) R-squared (without aggregation/grouping). 
@param input_data See doc of r_squared.
@param y See doc of r_squared.
@param yhat See doc of r_squared.
@param output_data See doc of r_squared.
@param lib See doc of r_squared.
@return 
@write 
*/
%macro r_squared_helper(input_data=, y=, yhat=, output_data=, lib=work);
    proc sql;
        create table &output_data as
        select 
            sum((yhat - ybar)**2) as ssr,
            sum((y - yhat)**2) as sse,
            sum((y - ybar)**2) as sst,
            mean((y - yhat)**2) as mse,
            calculated ssr / calculated sst as r_squared
        from ( 
            select 
                mean(&y) as ybar, 
                &y as y, 
                &yhat as yhat 
            from 
                &input_data
        )
        ;
    quit;
%mend;

