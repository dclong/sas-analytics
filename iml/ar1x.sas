/* include dependency code */
%include "~/sas/linear_regression.sas";

/*

@param xnew a data matrix containing new data (variables) neede for AR1X prediction.
It is typically a CCAR scenario,
however it can also be (part of, typeically last several quarters) historical 
predictor data for back-testing purpose.
The intercept column must be included in xnew.
The order of variables in scenario must be consistent with the coefficient input (b).
I use the convention that a variable starts with underscore is a output variable,
that is it will be changed afte this module is called.

@param lag1_index the index of column corresponding to the AR(1) term. 

@param b a column vector containing estimate of coefficients in the AR1X model.

*/
start predict_ar1x(xnew, lag1_index, b);
    _xnew = xnew;
    n = nrow(_xnew);
    y = j(n, 1, 0);
    do i = 1 to n by 1;
        y[i] = _xnew[i, ] * b;
        if i < n then do;
            _xnew[i+1, lag1_index] = y[i];
        end;
    end;
    return(y);
finish;

/*
Simulate 1 trajectory of the specified AR1X process.

@param x a data matrix containing data (variables) needed for AR1X simulation.

@param lag1_index the index of column corresponding to the AR(1) term. 

@param b a column vector containing estimate of coefficients in the AR1X model.

@param sigma the standard deviation of the error term.

Note that the AR1X model used here is specified as y_t = y_{t-1} + XB + e,
which is different from the standard text book specification.
This non-school version of AR1X is commonly used in banks.

*/
start sim_ar1x_1(x, lag1_index, b, sigma);
    _x= x;
    n = nrow(_x);
    y = j(n, 1, 0);
    do i = 1 to n by 1;
        y[i] = _x[i, ] * b + rand('NORMAL', 0, sigma);
        if i < n then do;
            _x[i+1, lag1_index] = y[i];
        end;
    end;
    return(y);
finish;
/*
Simulate multiple trajectories from the specified AR1X process.

@param sim_size the number of trajectories to simulate.

*/
start sim_ar1x(x, lag1_index, b, sigma, sim_size);
    sims = j(nrow(x), sim_size, 0);
    do i = 1 to sim_size by 1;
        sims[, i] = sim_ar1x_1(x, lag1_index, b, sigma);
    end;
    return(sims);
finish;



/*
Back-testing on AR1X model.

@param y a column vector containing historical response variable data. 
No scenario data should be included in y.

@param x a data matrix containing historical dependent variables data. 
No scenario data should be included in x.
Note that x must inlcude the intercept column.

@param lag1_index the column index of x corresponding to the AR(1) term.

@param back the number of quarters to go back.

@return the predicted response variable on the last back quarters.
The predicted values can be compared with the actual ones to assess the model.

@TODO: this module does not do model/lag selection but is based on the same 
model/lag decided previously. 
You probably need to make sure this is the right way to do it.
*/
start backtesting_ar1x(y, x, lag1_index, back);
	n = nrow(x);
	/*
	get training data set
	*/
	n_training = n - back;
	y_training = y[1:n_training];
	x_training = x[1:n_training, ];
	/*
	fit AR1X model
	*/
	b = bhat(y_training, x_training);
	/*
	testing data
	*/
	x_testing = x[(n_training+1):n, ];
	/*
	predict using the module predict_ar1x
	*/
	y_predict = predict_ar1x(x_testing, lag1_index, b);
	return(y_predict);	
finish;
/*
@param y a row/column vector containing historical response variable.

@param x a data matrix containing historical predictor variables. 
It must include the complete AR(1) term if you use a AR(1) model.

@param y_pred a row/column vector containing predicted response variable. 
This is typical the CCAR severly adverse scenario prediction.

@return partial historical predictor variables for comparison with the ccar scenarios. 
*/
start trace_back(y, x, y_pred);
	pred_max_index = y_pred[<:>];
	hist_max_index = y[<:>];
	hist_start_index = hist_max_index - pred_max_index + 1;
	return(x[hist_start_index:hist_max_index, ]);
finish;
