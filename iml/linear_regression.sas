
/*
@param x a matrix.
@return a matrix with a column of 1's prepended to x 
if its first column is not a column of 1's.
*/
start add_intercept(x);
    if any(x[, 1] ^= 1) then do;
        return(insert(j(nrow(x), 1, 1), x, 0, 2));
    end;
    return(x);
finish;

/*
Estimate the parameters in a simple linear regression.

@param y a column vector containing the response variable.

@param x a data matrix containing predictors.
It must contain the intercept column.

@return the estimate of the parameters in the simple linear regression.
*/
start bhat(y, x);
	return(inv(t(x) * x) * t(x) * y);
finish;

/*
Calculate fitted values in a simple linear regression.

@param x the data/desgin matrix in the simple linear regression.

@param bhat the estimate of parameters in the simple linear regresssion.
The order of columns in x must be consistent with bhat.

@return the fitted value of the simple linear regression.
*/
start yhat(x, bhat);
	return(x * bhat);
finish;
/*
Estimate the standard deviation of error in a simple linear regression.

@param y a column vector containing the response variable.

@param x a data matrix containing predictors.
*/
start sigma_hat(y, x);
    b = bhat(y, x);
    yhat = x * b;
    df = nrow(x) - ncol(x);
    sigma = sqrt(sum((y-yhat)##2)/df);
    return(sigma);
finish;

