
/**
Calculate the realized default rate for a given IA portfolio account.

@param segment_id the segment ID of this IA portfolio account.

@param segments a row/column vector containing all possible 6-upper-case-character 
(1-6 characters of the segment_id) realized default rate segments in the Indirect Auto portfolio.
For the Indirect Auto portfolio, it is typically (but subject to change if the models are changed) the following list.

            IAN_01
            IAN_02
            IAN_03
            IAN_04
            IAN_05
            IAN_06
            IAU_01
            IAU_02
            IAU_03
            IAU_04
            IAU_05
            IAU_06
            IAU_WO
            IAN_WO
            IAU_DF
            IAN_DF
            IAU_99
            IAN_99
            IAU_CL
            IAN_CL

@param realized_dr_seg a row/column vector containing realized default rates 
corresponding to the segments vector.

@return the realized default rate (a scalar) for this IA portfolio account.
*/
start realized_dr_seg_ia_1(segment_id, segments, realized_dr_seg);
    seg_id = strip(upcase(segment_id));
    dr_seg_id = substr(seg_id, 1, 6);
    dr_index = (dr_seg_id=segments);
    dr = .;
    if any(dr_index) then do;
        dr = realized_dr_seg[loc(dr_index)];
    end;
    return(dr);
finish;

/**
Calculate the realized default rates for given IA portfolio accounts.

@param segment_id a row/column vector containing the 10-character risk segment ID's for the IA portfolio accounts.

@param segments a row/column vector containing all possible 6-character 
(1-6 characters of the segment_id) realized default rate segments in the Indirect Auto portfolio.
For the Indirect Auto portfolio, it is typically (but subject to change if the models are changed) the following list.

            IAN_01
            IAN_02
            IAN_03
            IAN_04
            IAN_05
            IAN_06
            IAU_01
            IAU_02
            IAU_03
            IAU_04
            IAU_05
            IAU_06
            IAU_WO
            IAN_WO
            IAU_DF
            IAN_DF
            IAU_99
            IAN_99
            IAU_CL
            IAN_CL

@param realized_dr_seg a row/column vector containing realized default rates 
corresponding to the segments vector.

@return a column vector containing the realized default rates for the corresponding IA portfolio accounts.

*/
start realized_dr_seg_ia(segment_id, segments, realized_dr_seg);
    n = nrow(segment_id) * ncol(segment_id);
    dr = j(n, 1, 0);
    do i = 1 to n by 1;
        dr[i] = realized_dr_seg_ia_1(segment_id[i], segments, realized_dr_seg);
    end;
    return(dr);
finish;

/* 
Calculate PD scalar for the given IA portfolio accounts.

@param segment_id a row/column vector containing the 10-character risk segment ID's for the IA portfolio accounts.

@param pd_scalar a row/column vector containing the PD scalars for accounts starting with "IA".
The logic depends on how PD for IA is modeled.
The IA accounts that do not start with "IA" are not modeled current.

@details There are 2 segments for IA PD model. 
One segment is all acounts starting with "IA",
and the other segment is all other IA account (accounts that do not start with "IA").
The latter segment is not modeled.

@return the PD scalars for the IA portfolio accounts.
*/

start pd_scalar_seg_ia(segment_id, pd_scalar);
    n = nrow(segment_id) * ncol(segment_id);
    scalar = j(n, nrow(pd_scalar) * ncol(pd_scalar), 1);
    do i = 1 to n by 1;
        scalar_i = pd_scalar_seg_ia_1(segment_id[i], pd_scalar);
        scalar[i, ] = rowvec(scalar_i);
    end;
    return(scalar);
finish;

/* 
Calculate PD scalar for one IA portfolio account.

@param segment_id the 10-character risk segment ID's for the IA portfolio account.

@param pd_scalar a row/column vector containing the PD scalars for accounts starting with "IA".
The logic depends on how PD for IA is modeled.
The IA accounts that do not start with "IA" are not modeled current.

@return the PD scalar for this IA portfolio account.

*/
start pd_scalar_seg_ia_1(segment_id, pd_scalar);
    seg_id = strip(upcase(segment_id));
    scalar = pd_scalar;
    if substr(seg_id, 1, 2) ^= "IA" then do;
        /*
        The non-IA accounts are not modeled but applied to the model built on IA accounts,
        so the factor 1.03 is used to make it conservative
        */
        scalar = scalar * 1.03;
    end;
    return(scalar);
finish;


/**

Calculate realized LGD for one IA portfolio account.

@param segment_id the 10-character risk segment ID's for the IA portfolio account.

@param segments a row/column vector containing all possible 6-character 
(1-4 + 9-10 characters of the segment_id) LGD (not scalar) segments in the Indirect Auto portfolio.
For the Indirect Auto portfolio, it is typically (but subject to change if the models are changed) the following list.

        IAN_01 
        IAN_02 
        IAU_01 
        IAU_02 


@param realized_lgd_seg a vector containing the segmentation realized LGDs corresponding to segments.

@return the realized lgd (scalar) for this IA portfolio account.
*/
start realized_lgd_seg_ia_1(segment_id, segments, realized_lgd_seg);  
    seg_id = strip(upcase(segment_id));
    seg_id = cat(substr(seg_id, 1, 4), substr(seg_id, 9, 2));
    lgd = .;
    lgd_index = (seg_id=segments);
    if any(lgd_index) then do;
        lgd = realized_lgd_seg[loc(lgd_index)];
    end;
    return(lgd);
finish;

/**

Calculate realized LGD for the given IA portfolio accounts.

@param segment_id the 10-character risk segment ID's for the IA portfolio accounts.

@param segments a row/column vector containing all possible 6-character 
(1-4 + 9-10 characters of the segment_id) LGD (not scalar) segments in the Indirect Auto portfolio.
For the Indirect Auto portfolio, it is typically (but subject to change if the models are changed) the following list.

        IAN_01 
        IAN_02 
        IAU_01 
        IAU_02 

@param realized_lgd_seg a vector containing the segmentation realized LGDs corresponding to segments.

@return a row/column vector containing the realized lgd for the IA portfolio accounts.
*/
start realized_lgd_seg_ia(segment_id, segments, realized_lgd_seg);
    n = nrow(segment_id) * ncol(segment_id);
    lgd = j(n, 1, 0);
    do i = 1 to n by 1;
        lgd[i] = realized_lgd_seg_ia_1(segment_id[i], segments, realized_lgd_seg);  
    end;
    return(lgd);
finish;


/**

Calculate LGD scalars for one IA portfolio account.

@param segment_id the segment ID's for the IA portfolio account.

@param segments a row/column vector containing all possible 6-character 
(1-4 + 9-10 characters of the segment_id) LGD scalar segments in the Indirect Auto portfolio.
For the Indirect Auto portfolio, it is typically (but subject to change if the models are changed) the following list.

        IAN_01 
        IAN_02 
        IAU_01 
        IAU_02 

@param realized_lgd_seg the segmentation realized LGDs corresponding to segments.

@return the realized lgd (scalar) for this IA portfolio account.
*/
start lgd_scalar_seg_ia_1(segment_id, segments, lgd_scalar);  
    seg_id = strip(upcase(segment_id));
    seg_id = cat(substr(seg_id, 1, 4), substr(seg_id, 9, 2));
    scalar = j(nrow(lgd_scalar), 1, .);
    scalar_index = (seg_id=segments);
    if any(scalar_index) then do;
        scalar = lgd_scalar[, loc(scalar_index)];
    end;
    return(scalar);
finish;

/**

Calculate LGD scalars for the given IA portfolio accounts.

@param segment_id a row/column vector containing the segment ID's for the IA portfolio accounts.

@param segments a row/column vector containing all possible 6-character 
(1-4 + 9-10 characters of the segment_id) LGD scalar segments in the Indirect Auto portfolio.
For the Indirect Auto portfolio, it is typically (but subject to change if the models are changed) the following list.

        IAN_01 
        IAN_02 
        IAU_01 
        IAU_02 

@param realized_lgd_seg the segmentation realized LGDs corresponding to segments.

@return a matrix containing the lgd scalar for the IA portfolio accounts
with row being accounts and columns being time in quarter in order.
*/
start lgd_scalar_seg_ia(segment_id, segments, lgd_scalar);
    n = nrow(segment_id) * ncol(segment_id);
    scalar = j(n, nrow(lgd_scalar), 1);
    do i = 1 to n by 1;
        scalar_i = lgd_scalar_seg_ia_1(segment_id[i], segments, lgd_scalar);  
        scalar[i, ] = rowvec(scalar_i);
    end;
    return(scalar);
finish;

