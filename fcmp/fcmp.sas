proc fcmp outlib=work.funcs.trial;
    /*
    @author Ben Chuanlong Du
    Generate an integer in [0, n) with equal probability. 
    @param n the upper (exclusive) limit of the integer range [0, n).
    @return a randomly generated integer in [0, n).
    */
	function rand_int(n);
		return(floor(rand("Uniform") * n));
	endsub;
    /*
    @author Ben Chuanlong Du
    @description Generate a valid SAS name for a temporary variable/dataset. 
    @return a string starts with "_" and followed by 31 randomly generated digits.
    */
	function temp_name()$32;
		length rv $32;
		rv = "_";
		do i = 1 to 32;
			rv = cat(strip(rv), rand_int(10));
		end;
		return(rv);
	endsub;
    /*
    @author Ben Chuanlong Du
    @description Generate valid SAS names (separated by spaces) for temporary variables/datasets. 
    @param n the number of temporary names to generate.
    @return Valid temporary SAS names (see temp_name for details) separated by spaces.
    */
	function temp_names(n)$7911; /* 7911 is the max length in SAS 9.2 */
		length rv $7911;
		rv = "";
		do i = 1 to n;
			rv = cat(strip(rv), " ", strip(temp_name()));
		end;
		return(rv);
	endsub;
    function recover_year(year, lower_limit, upper_limmit);
        if lower_limit <= year <= upper_limmit then do;
            return(year);
        end;
        year_plus_1900 = year + 1900;
        if lower_limit <= year_plus_1900 <= upper_limmit then do;
            return(year_plus_1900);
        end;
        year_plus_2000 = year + 2000;
        if lower_limit <= year_plus_2000 <= upper_limmit then do;
            return(year_plus_2000);
        end;
        return(year);
    endsub;
    function quarter_of_month(month);
        return(ceil(month/3));
    endsub;
    function quarter_of_date(date);
        return(ceil(month(date)/3));
    endsub;
    function as_ddmmmyyyy(x);
        return(putn(x, "date9."));
    endsub;
    function as_ddmmmyy(x);
        return(putn(x, "date7."));
    endsub;
    /* think of _ as / in the output */
    function as_yyyy_mm_dd(x);
        return(putn(d, "yymmdd10."));
    endsub;
    function as_mm_dd_yyyy(x);
        return(putn(d, "mmddyy10."));
    endsub;
    function as_dd_mm_yyyy(x);
        return(putn(d, "ddmmyy10.")) 	
    endsub;
    function as_yy_mm_dd(x);
        return(putn(d, "yymmdd8."));
    endsub;
    function as_yyyymmdd(x);
        return(putn(d, "yymmddn8."));
    endsub;
    function as_mm_dd_yy(x);
        return(putn(d, "mmddyy8."));
    endsub;
    function as_mmddyyyy(x);
        return(putn(d, "mmddyyn8."));
    endsub;
    function as_dd_mm_yy(x);
        return(putn(d, "ddmmyy8."));
    endsub;
    function as_ddmmyyyy(x);
        return(putn(d, "ddmmyyn8."));
    endsub;
    function as_yymmdd(x);
        return(putn(d, "yymmdd6."));
    endsub;
    function as_mmddyy(x);
        return(putn(d, "mmddyy6."));
    endsub;
    function as_ddmmyy(x);
        return(putn(d, "ddmmyy6."));
    endsub;
    function as_yyyymm(x);
        return(putn(d, "yymmn."));
    endsub;
    function as_mmyyyy(x);
        return(putn(d, "mmyyn."));
    endsub;
    function pd_q2y(p_quarterly);
        p = 1 - (1-p_quarterly)##4;
        return(p);
    endsub;
    function pd_y2q(p_yearly);
        p = 1 - (1-p_yearly)##0.25;
        return(p);
    endsub;
    function growth_q2y(g_quarterly);
        g = (1+g_quarterly)##4 - 1;
        return(g);
    endsub;
    function growth_y2q(g_yearly);
        g = (1+g_yearly)##0.25 - 1;
        return(g);
    endsub;
    function pd_scalar(dr_stressed, dr_actual, moc);
        return(dr_stress / dr_actual * moc);
    endsub;
    function cap_floor(x, cap, floor);
        if x > cap then do;
            return(cap);
        end;
        if x < floor then do;
            return(floor);
        end;
        return(x);
    endsub;
run;

options cmplib=work.funcs;

