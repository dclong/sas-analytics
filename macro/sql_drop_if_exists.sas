/*
Drop a table if it exists. 
It is intended to be used in PROC SQL.
You can use the drop statement to drop a table/view directly.
The advantage of this macro is that it will not try to delete a table/view
if it does not exist and thus will not trigger warning messages.
It is much cleaner.
@param object a table or view to be dropped.
*/
%macro sql_drop_if_exists(object);
    %if %sysfunc(exist(&object, data)) %then
        drop table &object
        ;
    %else %if %sysfunc(exist(&object, view)) %then
        drop view &object
        ;
%mend;
